﻿#include "Math.h"


long long Math::FindPowerOfNumberUsingIterativeMethod(int OriginalNumber, int Degree)
{
    if (Degree == 1 || OriginalNumber == 0)
    {
        return OriginalNumber; 
    }
    else if (Degree == 0)
    {
        return 1;
    }

    long long Number = OriginalNumber;
    for (int i = 1; i < Degree; i++)
    {
        Number *= OriginalNumber; 
    }
    return Number;
}

long long Math::FindFibonacciNumberUsingRecursiveMethod(long Number)
{
    if(Number <= 1)
    {
        return Number;
    }

    return FindFibonacciNumberUsingRecursiveMethod(Number - 1) + FindFibonacciNumberUsingRecursiveMethod(Number - 2);
}

long long Math::FindFibonacciNumberUsingIterativeMethod(int Number)
{
    if (Number <= 1)
    {
        return Number;
    }

    const int StartNamberWith = 3;
    long long FirstNumber = 1, SecondNumber = 1, Sum = 0;
    
    for (int i = StartNamberWith; i <= Number; i++)
    {
        Sum = FirstNumber + SecondNumber;
        FirstNumber = SecondNumber;
        SecondNumber = Sum;
    }
    
    return Sum;
}

long long Math::FinQuantityOfPrimeNumbers(long Range)
{
    long QuantityOfPrimeNumbers = 0;
    for (int Number = 1; Number <= Range; Number++)
    {
        if (IsPrimeNumber(Number))
        {
            QuantityOfPrimeNumbers++;
        }
    }
    return QuantityOfPrimeNumbers;
}

bool Math::IsPrimeNumber(long Number)
{
    int QuantityDividers = 0;
    for (int j = 1; j <= Number; j++)
    {
        if (Number % j == 0)
        {
            QuantityDividers++;
        }
    }
    
    bool Result = QuantityDividers == 2;
    return Result;
}

