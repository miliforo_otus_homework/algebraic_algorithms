﻿#pragma once

class Math
{
public:
    static long long FindPowerOfNumberUsingIterativeMethod(int OriginalNumber, int Degree);
    static long long FindFibonacciNumberUsingRecursiveMethod(long Number);
    static long long FindFibonacciNumberUsingIterativeMethod(int Number);
    static long long FinQuantityOfPrimeNumbers(long Number);
    static bool IsPrimeNumber(long Number);
};
