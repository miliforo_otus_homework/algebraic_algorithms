

#include <iostream>

#include "Math.h"

using namespace std;

int main(int argc, char* argv[])
{
    cout << Math::FindPowerOfNumberUsingIterativeMethod(2, 8) << endl;
    cout << Math::FindFibonacciNumberUsingIterativeMethod(11) << endl;
    cout << Math::FindFibonacciNumberUsingRecursiveMethod(11) << endl;
    cout << Math::FinQuantityOfPrimeNumbers(100) << endl;
    return 0;
}
